import { OnInit, AfterViewInit, EventEmitter } from '@angular/core';
import { GridOptions } from "ag-grid/main";
import { ControlValueAccessor } from "@angular/forms/src/forms";
export declare class AtListsComponent implements OnInit, AfterViewInit, ControlValueAccessor {
    gridOptions: GridOptions;
    private onTouchedCallback;
    private onChangeCallback;
    selection: string;
    showActions: boolean;
    enableFilter: boolean;
    enableSorting: boolean;
    listLevelActions: boolean;
    selectedRows: EventEmitter<any>;
    constructor();
    ngOnInit(): void;
    fetchData(): void;
    ngAfterViewInit(): void;
    getColumns(): any;
    getColDef(): any;
    getData(): any;
    writeValue(value: any): void;
    registerOnChange(fn: any): void;
    registerOnTouched(fn: any): void;
    dateRenderer(params: any): string;
    onFilterChanged(value: any): void;
    getAvailableActions(): Array<any>;
    private actionsColDef;
}
