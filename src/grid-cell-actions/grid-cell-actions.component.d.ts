import { AfterViewInit } from '@angular/core';
import { ICellRendererAngularComp } from "ag-grid-angular/main";
export declare class GridCellActionsComponent implements ICellRendererAngularComp, AfterViewInit {
    params: any;
    actions: any;
    context: any;
    data: any;
    ngAfterViewInit(): void;
    agInit(params: any): void;
    invokeParentAction(action: any): void;
    refresh(params: any): boolean;
}
