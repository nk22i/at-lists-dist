import { AtListsComponent } from './at-lists.component';
import { OnInit } from '@angular/core';
import { GridOptions } from "ag-grid/main";
export declare class AtListsServerComponent extends AtListsComponent implements OnInit {
    gridOptions: GridOptions;
    constructor();
    ngOnInit(): void;
    fetchData(): void;
    getDataSource(): any;
}
